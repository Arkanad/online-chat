using OnlineChat.DTO;
using OnlineChat.Models;

namespace OnlineChat.Interfaces;

public interface IUserRepository
{
    Task<IEnumerable<Member>> GetUsersAsync();
    Task<Member?> GetUserByIdAsync(int id);
    Task<Member?> GetUserByUsernameAsync(string? username);
    Task<bool> SaveAllAsync();
    Task<Member?> GetUserByEmailAsync(string? email);

    Task<int> GetUserIdByUsernameAsync(string? username);
}