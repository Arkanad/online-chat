using Microsoft.AspNetCore.Mvc;
using OnlineChat.DTO;
using OnlineChat.Models;

namespace OnlineChat.Interfaces;

public interface IAccountService
{
    public Task<UserDto> Register(RegisterDto registerDto);

    public Task<UserDto> Login(LoginDto loginDto);

    public Task<bool> IsEmailTaken(string? email);

    public Task<bool> IsUsernameTaken(string? username);
}