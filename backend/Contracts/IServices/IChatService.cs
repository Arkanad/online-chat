﻿using OnlineChat.Models;

namespace OnlineChat.Interfaces;

public interface IChatService
{
    Task<int> GetChatIdByName(string chatName);  
    Task<int?> GetChatIdByUserIds(List<int> userIds); 
    Task JoinChatAsync(NewChatRequestDto newChatRequestDto, string currentUserUsername);  
    Task<List<int>> GetChatsIdsByUserId(int userId);
    Task<Dictionary<int, List<int>>> GetUserIdsByChatIdsAsync(List<int> chatIds);
    Task<List<Member>> GetRecentChats(Dictionary<int, List<int>> chatAndUsers, int currentUserId); 
    Task AddUsersToChatAsync(int chatId, string userId);
}