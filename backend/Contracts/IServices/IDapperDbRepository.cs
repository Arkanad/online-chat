using System.Data;

namespace OnlineChat.Interfaces;

public interface IDapperDbRepository
{
    public IDbConnection CreateConnection();
}