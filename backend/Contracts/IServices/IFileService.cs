﻿using OnlineChat.DTO;
using OnlineChat.Models;

namespace OnlineChat.Interfaces;

public interface IFileService
{
    Task<string> Upload(Image image, string containerName);
    
    Task<Stream> Get(String name, string containerName);

    Task SaveFileToDb(FileDTO fileDto);
}