using OnlineChat.DTO;

namespace OnlineChat.Interfaces;

public interface IHashingPassword
{
    public Task<string> CreateUser(RegisterDto create);
    public Task<string> UserVerify(RegisterDto verify);
}