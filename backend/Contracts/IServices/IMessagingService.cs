﻿using Microsoft.AspNetCore.Mvc;
using OnlineChat.DTO;
using OnlineChat.Models;

namespace OnlineChat.Interfaces;

public interface IMessagingService
{
    Task<int> SendMessageAsync(MessageDto messageDto);
    Task<IEnumerable<MessageDto>> LoadChatHistory(int chatId);
    Task<Message> GetMessageByIdAsync(string messageId);
    Task DeleteMessage(int messageId);
    Task SelectMessage(int messageId);
    Task<Message> EditMessage(int messageId);

}