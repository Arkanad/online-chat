﻿using System.Threading.Tasks;
using OnlineChat.Models;

namespace OnlineChat.Interfaces
{
    public interface ISessionService
    {
        Task AddSessionAsync(string username, string chatRoom, string connectionId);
        Task RemoveSessionAsync(string connectionId);
        Task<int?> GetUserIdByConnectionIdAsync(string connectionId);
        Task<UserConnection> GetSessionByConnectionIdAsync(string connectionId);
        Task<UserConnection> GetSessionByContextConnectionId(string contextConnectionId);
        Task<IEnumerable<int>> GetOnlineUserIdsAsync();

        Task<bool> isSessionExist(string contextConnectionId);
    }

}