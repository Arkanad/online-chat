using OnlineChat.Models;

namespace OnlineChat.Interfaces;

public interface ITokenService
{
    Task<string> CreateToken(User appUser);

    int GetUserIdFromToken(string token);
}