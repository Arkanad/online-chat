﻿using OnlineChat.DTO;

namespace OnlineChat.Contracts.Interfaces;

public interface IChatClient
{
    public Task ReceiveMessage(string? username, string message, string sentAt);
    Task SendAsync(string message,List<MessageDto> messages);
    public Task LoadChatHistory(List<MessageDto> messages);
    Task ReceiveFileMessage(string username, string message, string fileName, string timestamp);
}