using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineChat.DTO;
using OnlineChat.Interfaces;
using OnlineChat.Services;
using Microsoft.AspNetCore.Http;

namespace OnlineChat.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AccountController : BaseApiController
{
    private readonly IAccountService _accountService;
    private readonly RedisService _redisService;
    private readonly IUserRepository _userRepository;

    public AccountController(IAccountService accountService, RedisService redisService, IUserRepository userRepository)
    {
        _accountService = accountService;
        _redisService = redisService;
        _userRepository = userRepository;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<ActionResult<UserDto>> Login([FromBody] LoginDto loginDto)
    {
        var responseUser = await _accountService.Login(loginDto);
        if (responseUser != null)
        {
            HttpContext.Response.Cookies.Append(".AspNetCore.Application.Id", responseUser.Token,
                new CookieOptions
                {
                    MaxAge = TimeSpan.FromMinutes(1000),
                    Secure = false, // Set to true if using HTTPS2
                    SameSite = SameSiteMode.Strict, // Adjust if necessary
                    Path = "/"
                });
            HttpContext.Response.Cookies.Append("UserUsername", _userRepository.GetUserByEmailAsync(loginDto.Email).Result.Username,
                new CookieOptions
                {
                    MaxAge = TimeSpan.FromMinutes(1000),
                    Secure = false, // Set to true if using HTTPS2
                    SameSite = SameSiteMode.Strict, // Adjust if necessary
                    Path = "/"
                });
            var currentUser = _userRepository.GetUserByEmailAsync(responseUser.Email);
            await _redisService.SetUserOnlineAsync(currentUser.Result.UserId);
            return Ok(responseUser);
        }
        else
            return BadRequest("Email or password is invalid!");
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<ActionResult<UserDto>> Register([FromBody] RegisterDto registerDto)
    {
        if (registerDto != null)
        {
            if (await _accountService.IsEmailTaken(registerDto.Email))
            {
                return Unauthorized(new
                    { isValid = false, errorMessage = $"Email {registerDto.Email} is already in use." });
            }

            if (await _accountService.IsUsernameTaken(registerDto.Username))
                return Unauthorized(new
                    { isValid = false, errorMessage = $"Username {registerDto.Username} is already in use." });
            var registrationResult = await _accountService.Register(registerDto);
            if (registrationResult != null)
            {
                HttpContext.Response.Cookies.Append(".AspNetCore.Application.Id", registrationResult.Token,
                    new CookieOptions
                    {
                        MaxAge = TimeSpan.FromMinutes(120),
                        HttpOnly = true,  
                        Secure = true
                    });
                var currentUser = _userRepository.GetUserByEmailAsync(registerDto.Email);
                await _redisService.SetUserOnlineAsync(currentUser.Result.UserId);
                return Ok(registrationResult);
            }
            else
                return BadRequest("Registration failed!");
        }
        else
        {
            return Unauthorized("Invalid data!");
        }
    }

    [Authorize]
    [HttpPost("logout")]
    public async Task<ActionResult<UserDto>> Logout([FromBody] string? CurrentUserUsername)
    {
        var currentUser = _userRepository.GetUserByUsernameAsync(CurrentUserUsername);
        await _redisService.SetUserOfflineAsync(currentUser.Result.UserId);
        
        HttpContext.Response.Cookies.Append(".AspNetCore.Application.Id", "", new CookieOptions
        {
            Expires = DateTimeOffset.UtcNow.AddDays(-1) // Set expiration to the past
        });
        return Ok(new { message = "Logout successful." });
    }
}