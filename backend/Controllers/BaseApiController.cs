using Microsoft.AspNetCore.Mvc;

namespace OnlineChat.Controllers;

[ApiController]
[Route("[controller]")]
public class BaseApiController : ControllerBase
{
    
}