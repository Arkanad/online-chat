﻿using Microsoft.AspNetCore.Mvc;
using OnlineChat.Interfaces;

namespace OnlineChat.Controllers;

public class ChatController : BaseApiController
{
    private IUserRepository _userRepository;
    private IMessagingService _messagingService;
    private IChatService _chatService;

    ChatController(IChatService chatService, IUserRepository userRepository, IMessagingService messagingService)
    {
        _chatService = chatService;
        _userRepository = userRepository;
        _messagingService = messagingService;
    }
    
    [HttpGet("GetAllChats")]
    public async Task<IActionResult> GetAllChats([FromQuery] string? currentUserUsername)
    {
        var token = Request.Cookies[".AspNetCore.Application.Id"];
        try
        {
            int currentUserId = await _userRepository.GetUserIdByUsernameAsync(currentUserUsername);
            var chatsIdsOfUser = await _chatService.GetChatsIdsByUserId(currentUserId);
            var chatAndUsers = await _chatService.GetUserIdsByChatIdsAsync(chatsIdsOfUser);
            var recentChats = await _chatService.GetRecentChats(chatAndUsers, currentUserId);
            return Ok(recentChats);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error in GetAllChats: {ex.Message}");
            return BadRequest(new { Message = "Failed to fetch chats.", Error = ex.Message });
        }
    }
    
    
}