﻿using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Npgsql;
using OnlineChat.DTO;
using OnlineChat.Hubs;
using OnlineChat.Interfaces;
using OnlineChat.Models;
using OnlineChat.Services;
using StackExchange.Redis;

namespace OnlineChat.Controllers;

[Authorize]
[ApiController]
[Route("api/[controller]")]
public class MessagingController : BaseApiController
{
    private IMessagingService _messagingService;
    private RedisService _redisService;
    private IUserRepository _userRepository;
    private readonly IConfiguration _configuration;
    private readonly IFileService _fileService;

    private const string AzureContainerName = "main";

    public MessagingController(IMessagingService messagingService, RedisService redisService,
        IUserRepository userRepository, IConfiguration configuration, IFileService fileService)
    {
        _messagingService = messagingService;
        _redisService = redisService;
        _userRepository = userRepository;
        _configuration = configuration;
        _fileService = fileService;
    }
    
    public Task<ActionResult<Message>> GetMessage()
    {
        return null;
    }

    public async Task<IActionResult> GetMessagesAsync(string chatId)
    {
        var messages = await _redisService.GetMessagesAsync(chatId);
        return Ok(messages);
    }

    [HttpPost("Upload")]
    public async Task<IActionResult> UploadImage([FromForm] Image image)
    {
        int currentUserId = await _userRepository.GetUserIdByUsernameAsync(Request.Cookies["UserUsername"]);
        int chatId = await _redisService.GetCurrentChatIdAsync(currentUserId);
       
        
        if (image == null || image.ImageFile == null)
        {
            return BadRequest("File model or image file is missing.");
        }
    
        try
        {
            string url = await _fileService.Upload(image, AzureContainerName);
            FileDTO fileDto = new FileDTO()
            {
                ChatId = chatId,
                FileName = image.ImageFile.FileName,
                FileSize = int.Parse(image.ImageFile.Length.ToString()),
                FileType = image.ImageFile.ContentType,
                SenderId = currentUserId,
                FileUrl = url
            };
            await _fileService.SaveFileToDb(fileDto);
            return Ok(new { message = "File uploaded successfully", fileUrl = url, FileType = image.ImageFile.ContentType  });
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }
    
    [HttpGet("download")]
    public async Task<IActionResult> Download(String name)
    {
        var imageFileStream = await _fileService.Get(name ,AzureContainerName);
        string fileType = "jpeg";
        if (name.Contains("png"))
        {
            fileType = "png";
        }
        return File(imageFileStream, $"image/{fileType}", $"blobfile.{fileType}");
    }

    private async Task SaveFileMetadataToDatabase(string fileName, string fileUrl, string description)
    {
        string connectionString = _configuration.GetConnectionString("PostgreSql");

        using (var connection = new NpgsqlConnection(connectionString))
        {
            await connection.OpenAsync();

            // Insert the file metadata into PostgreSQL
            string query = "INSERT INTO file_metadata (file_name, file_url, description, upload_date) " +
                           "VALUES (@FileName, @FileUrl, @Description, @UploadDate)";

            using (var cmd = new NpgsqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("FileName", fileName);
                cmd.Parameters.AddWithValue("FileUrl", fileUrl);
                cmd.Parameters.AddWithValue("Description", description);
                cmd.Parameters.AddWithValue("UploadDate", DateTime.UtcNow);

                await cmd.ExecuteNonQueryAsync();
            }
        }
    }
}

