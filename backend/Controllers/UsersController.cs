using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineChat.Interfaces;
using OnlineChat.Services;

namespace OnlineChat.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly RedisService _redisService;

        public UsersController(IUserRepository userRepository, RedisService redisService)
        {
            _userRepository = userRepository;
            _redisService = redisService;
        }

        [HttpGet("GetAllUsers")]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _userRepository.GetUsersAsync();
            return Ok(users);
        }

        [HttpGet("GetByUsername/{username}")]
        public async Task<IActionResult> GetUserByUsername(string? username)
        {
            var result = await _userRepository.GetUserByUsernameAsync(username);
            return Ok(result);
        }

        [HttpGet("GetByEmail/{email}")]
        public async Task<IActionResult> GetUserByEmail(string? email)
        {
            var result = await _userRepository.GetUserByEmailAsync(email);
            if (result != null)                    
                return Ok(result);
            else
                return Unauthorized();
        }

        [HttpGet("GetUserOnlineStatus")]
        public async Task<IActionResult> GetUserOnlineStatus([FromQuery]int userId)
        {
            var isUserOnline = await _redisService.IsUserOnlineAsync(userId);
            return Ok(isUserOnline);
        }
    }
}