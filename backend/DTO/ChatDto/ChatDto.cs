﻿using System.Collections;

namespace OnlineChat.DTO;

public class ChatDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime CreatedAt { get; set; }
    public int CreatedBy { get; set; }
    public ICollection<int> Participants { get; set; }
    public ICollection<MessageDto> Messages { get; set; }
    public bool isGroup { get; set; }
}