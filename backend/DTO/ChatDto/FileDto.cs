﻿namespace OnlineChat.DTO;

public class FileDTO
{
    public int ChatId { get; set; }            // ID of the chat this file belongs to
    public int SenderId { get; set; }          // ID of the user who sent the file
    public string FileName { get; set; }        // Name of the file
    public string FileType { get; set; }        // Type of the file (e.g., 'image/png')
    public int FileSize { get; set; }          // Size of the file in bytes
    public string FileUrl { get; set; }         // URL or path to the file
}
