﻿using OnlineChat.Models;

namespace OnlineChat.DTO;

public class MessageDto
{
    public string ChatName { get; set; }
    public string? Username { get; set; }
    public int ChatId { get; set; }
    public int UserId { get; set; }
    public string Content { get; set; }
    public DateTime Sent_at { get; set; }
    
}