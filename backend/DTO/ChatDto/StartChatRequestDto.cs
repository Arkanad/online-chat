﻿namespace OnlineChat.Models;

public class NewChatRequestDto
{
    public int ContactId { get; set; }
    public string ChatRoom { get; set; }
}