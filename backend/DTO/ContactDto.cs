﻿namespace OnlineChat.DTO;

public class ContactDto
{
    public int Id { get; set; }
    
    public string? Name { get; set; }
    
    public string? Username { get; set; }
}