using System.ComponentModel.DataAnnotations;

namespace OnlineChat.DTO;

public class LoginDto
{
    [EmailAddress(ErrorMessage = "WRONG_EMAIL")]
    [Required] public string? Email { get; set; }
    
    
    [Required] public string Password { get; set; }
}