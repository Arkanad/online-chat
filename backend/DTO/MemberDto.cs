namespace OnlineChat.DTO;

public class MemberDto
{
    public int UserId { get; set; }
    public string Name { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
}