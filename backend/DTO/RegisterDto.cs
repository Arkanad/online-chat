using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace OnlineChat.DTO
{
    public class RegisterDto
    {
        [EmailAddress (ErrorMessage = "WRONG_EMAIL")]
        public string? Email { get; set; }

        [Required (ErrorMessage = "WRONG_USERNAME")] 
        public string? Username { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 7)]
        public string Password { get; set; }

        [Required] 
        public string? Name { get; set; }
    }
}