using System.IdentityModel.Tokens.Jwt;
using System.Runtime.InteropServices.JavaScript;
using System.Security.Claims;
using Azure.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using OnlineChat.Contracts.Interfaces;
using OnlineChat.DTO;
using OnlineChat.Interfaces;
using OnlineChat.Models;
using OnlineChat.Services;
using Microsoft.AspNetCore.Http;
using StackExchange.Redis;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace OnlineChat.Hubs
{
    public class ChatHub : Hub<IChatClient>
    {
        private readonly RedisService _redisService;
        private readonly IMessagingService _messagingService;
        private readonly IDistributedCache _cache;
        private readonly IUserRepository _userRepository;
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly IChatService _chatService;

        public ChatHub(IMessagingService messagingService, IDistributedCache cache, RedisService redisService,
            IUserRepository userRepository, IConnectionMultiplexer connectionMultiplexer, IChatService chatService)
        {
            _messagingService = messagingService;
            _redisService = redisService;
            _cache = cache;
            _userRepository = userRepository;
            _connectionMultiplexer = connectionMultiplexer;
            _chatService = chatService;
        }

        public async Task JoinChat(int contactId, string chatRoom, string? contactUserUsername)
        {
            try
            {
                var newChatRequestDto = new NewChatRequestDto()
                {
                    ChatRoom = chatRoom,
                    ContactId = contactId
                };
                var currentUserUsername = Context.GetHttpContext().Request.Cookies["UserUsername"];

                await Groups.AddToGroupAsync(Context.ConnectionId, chatRoom);
                var user = await _userRepository.GetUserByUsernameAsync(currentUserUsername);
                if (user == null)
                {
                    throw new InvalidOperationException("User not found.");
                }

                var userConnection = new UserConnection
                {
                    Id = user.UserId,
                    Username = user.Username,
                    ChatRoom = chatRoom
                };

                var stringConnection = JsonSerializer.Serialize(userConnection);
                await _cache.SetStringAsync(Context.ConnectionId, stringConnection);
                var chatId = await _chatService.GetChatIdByName(chatRoom);
                var messages = await _messagingService.LoadChatHistory(chatId);
                var messagesList = messages.ToList();
                int userId = await _userRepository.GetUserIdByUsernameAsync(currentUserUsername);
                await _redisService.SetCurrentChatIdAsync(userId, chatId);
                await Clients.Caller.LoadChatHistory(messagesList);
                await _chatService.JoinChatAsync(newChatRequestDto, contactUserUsername);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception in JoinChat: {ex.Message}");
                throw;
            }
        }
        
        public async Task<List<Member>> GetAllChats(int currentUserId)
        {
            var chatsIdsOfUser = await _chatService.GetChatsIdsByUserId(currentUserId);
            var chatAndUsers = await _chatService.GetUserIdsByChatIdsAsync(chatsIdsOfUser);
            var recentChats = await _chatService.GetRecentChats(chatAndUsers, currentUserId);
            return recentChats;
        }

        public async Task SendMessage(string msg)
        {
            var stringConnection = await _cache.GetAsync(Context.ConnectionId);

            var connection = JsonSerializer.Deserialize<UserConnection>(stringConnection);

            await Clients.Group(connection.ChatRoom)
                .ReceiveMessage(connection.Username, msg, DateTime.Now.ToString("o"));

            int userId = _userRepository.GetUserByUsernameAsync(connection.Username).Result.UserId;
            int chatId = await _chatService.GetChatIdByName(connection.ChatRoom);
            MessageDto messageDto = new MessageDto()
            {
                ChatName = connection.ChatRoom,
                Username = connection.Username,
                ChatId = chatId,
                UserId = userId,
                Content = msg,
                Sent_at = DateTime.Now
            };
            await _messagingService.SendMessageAsync(messageDto);
        }

        public async Task LoadChatHistory(string chatName)
        {
            try
            {
                var stringConnection = await _cache.GetStringAsync(Context.ConnectionId);
                var stringChatIdFromConnection = _cache.GetStringAsync("CurrentChatId");
                var chatId = await _chatService.GetChatIdByName(chatName);

                var connection = JsonSerializer.Deserialize<UserConnection>(stringConnection);

                if (connection == null)
                {
                    throw new InvalidOperationException("User connection not found.");
                }

                var messages = await _messagingService.LoadChatHistory(chatId);

                var retrievedMessages = messages.Select(message => new MessageDto
                {
                    Username = message.Username,
                    Content = message.Content
                }).ToList();

                await Clients.Caller.LoadChatHistory(retrievedMessages);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error in LoadChatHistory: {ex.Message}");
                throw;
            }
        }

        public async Task<UserConnection> LoadLastChat()
        {
            var connection = _cache.GetString(Context.ConnectionId);
            var retrievedConnection = JsonConvert.DeserializeObject<UserConnection>(connection);
            return new UserConnection()
            {
                Username = retrievedConnection.Username,
                ChatRoom = retrievedConnection.ChatRoom
            };
        }

        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            await base.OnDisconnectedAsync(exception);
        }

        public int GetUserIdFromToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();

            try
            {
                var jwtToken = handler.ReadJwtToken(token);
                var userIdClaim = jwtToken.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim == null)
                {
                    throw new InvalidOperationException("The token does not contain a user ID claim.");
                }

                if (int.TryParse(userIdClaim.Value, out int userId))
                {
                    return userId;
                }
                else
                {
                    throw new FormatException("User ID claim is not a valid integer.");
                }
            }
            catch (Exception ex)
            {
                throw new SecurityTokenException("Invalid token provided", ex);
            }
        }
    }
}