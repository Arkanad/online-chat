﻿using OnlineChat.DTO;

namespace OnlineChat.Models;

public class Chat
{
    public int Id { get; set; }
    public string ChatName { get; set; }
    public ICollection<Member> Participants { get; set; }
}