﻿namespace OnlineChat.Models;

public class File
{
    public int Id { get; set; }
    public int MessageId { get; set; }
    public string FileName { get; set; }
    public string FileType { get; set; }
    public string FileUrl { get; set; }
    public DateTime UploadTime { get; set; }

    // Navigation property
    public Message Message { get; set; }
}
