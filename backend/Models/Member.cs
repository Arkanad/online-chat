﻿namespace OnlineChat.Models;

public class Member
{
    public int UserId { get; set; }
    public string Name { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
}