namespace OnlineChat.Models;

public class User
{
    public int UserId { get; set; }
    public string? Username { get; set; }
    public string? Email { get; set; }
    public string? Name { get; set; }
    public DateTime? LastOnline { get; set; }
    public string Passwordhash { get; set; }
    public string Passwordsalt { get; set; }

}