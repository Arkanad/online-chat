﻿namespace OnlineChat.Models;

public record UserChat
{
    public int Id { get; set; }
    public int InitiatingUserId { get; set; }
    public IEnumerable<int> ContactUserIds { get; set; }
    public int ChatId { get; set; }
}