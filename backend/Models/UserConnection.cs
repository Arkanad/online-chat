﻿namespace OnlineChat.Models;

public class UserConnection
{
    public int Id { get; set; }
    public string? Username { get; set; } = string.Empty;
    public string ChatRoom { get; set; } = string.Empty;
}