using System.Data;
using Npgsql;
using OnlineChat.Interfaces;

namespace OnlineChat.Services;

public class DapperDbRepository : IDapperDbRepository
{
    public readonly string ConnectionString;

    public DapperDbRepository(IConfiguration configuration)
    {
        ConnectionString = configuration.GetConnectionString("DefaultConnection");
    }

    public IDbConnection CreateConnection()
    {
        return new NpgsqlConnection(ConnectionString);
    }
}