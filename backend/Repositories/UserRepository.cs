using System.Data;
using Dapper;
using Microsoft.AspNetCore.Http.HttpResults;
using OnlineChat.DTO;
using OnlineChat.Models;
using OnlineChat.Services;

namespace OnlineChat.Interfaces;

public class UserRepository : IUserRepository
{
    private readonly IDbConnection _dapperDbRepository;

    public UserRepository(IDapperDbRepository dapperDbRepository)
    {
        _dapperDbRepository = dapperDbRepository.CreateConnection();
    }

    public async Task<IEnumerable<Member>> GetUsersAsync()
    {
        return await _dapperDbRepository.QueryAsync<Member>("SELECT * FROM Users");
    }

    public async Task<Member?> GetUserByIdAsync(int userId)
    {
        var user =  _dapperDbRepository.QueryFirstOrDefault<Member>("SELECT * FROM Users where UserId=@userId", new { UserId = userId });
        return user;
    }

    public async Task<Member?> GetUserByUsernameAsync(string? username)
    {
        if (string.IsNullOrEmpty(username))
        {
            throw new ArgumentException("Username cannot be null or empty", nameof(username));
        }

        try
        {
            var query = "SELECT * FROM Users WHERE username = @username";
            var user = await _dapperDbRepository.QueryFirstOrDefaultAsync<Member>(query, new { Username = username });
        
            if (user == null)
            {
                Console.WriteLine($"No user found with username: {username}");
            }

            return user;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while getting user by username: {ex.Message}");
            throw; 
        }
    }



    public async Task<int> GetUserIdByUsernameAsync(string? username)
    {
        var trimmedUsername = username.Trim();
        var sql = "SELECT UserId FROM Users WHERE Username = @username";
        try
        {
            var userId = await _dapperDbRepository.QueryFirstOrDefaultAsync<int>(sql, new { Username = trimmedUsername });
        
            if (userId == 0)
            {
                Console.WriteLine($"User with username '{username}' not found.");
            }
            return userId;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error fetching user ID: {ex.Message}");
            throw;
        }
    }


    public Task<MemberDto?> GetMemberAsync(string username, bool isCurrentUser)
    {
        throw new NotImplementedException();
    }

    public async Task<bool> SaveAllAsync()
    {
        var query = @"
            INSERT INTO Users (UserId, Name, UserName, Email, Password)
            VALUES (@UserId, @Name, @UserName, @Email, @Password, @Roles::text[])
            ON CONFLICT (UserId) DO UPDATE
            SET Name = EXCLUDED.Name,
                UserName = EXCLUDED.UserName,
                Email = EXCLUDED.Email,
                Password = EXCLUDED.Password
        ";

        _dapperDbRepository.Open();
        using (var transaction = _dapperDbRepository.BeginTransaction())
        {
            try
            {
                await _dapperDbRepository.ExecuteAsync(query, transaction: transaction);
                transaction.Commit();
                return true;
            }
            catch
            {
                transaction.Rollback();
                return false;
            }
            finally
            {
                _dapperDbRepository.Close();
            }
        }
    }

    public async Task<Member?> GetUserByEmailAsync(string? email)
    {
        var userResponse = await _dapperDbRepository.QueryFirstOrDefaultAsync<Member>(
            "SELECT * FROM Users WHERE Email = @email",
            new { email = email });
        if (userResponse == null)
        {
            return null;
        }
        else
        {
            return userResponse;
        }
    }
}