﻿using System.Data;
using BCrypt.Net;
using Dapper;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using OnlineChat.DTO;
using OnlineChat.Interfaces;
using OnlineChat.Models;

namespace OnlineChat.Services;

public class AccountService : IAccountService, IAsyncDisposable
{
    private readonly IDbConnection _dbConnection;
    private readonly ITokenService _tokenService;
    private readonly RedisService _redisService;
    private readonly IUserRepository _userRepository;

    public AccountService(ITokenService tokenService, IDapperDbRepository dapperDbRepository, RedisService redisService, IUserRepository userRepository)
    {
        _tokenService = tokenService;
        _dbConnection = dapperDbRepository.CreateConnection();
        _redisService = redisService;
        _userRepository = userRepository;
    }

    public async Task<UserDto> Register(RegisterDto registerDto)
    {
        string passwordSalt;
        string passwordHash;

        if (registerDto == null)
        {
            return null;
        }

        passwordSalt = BCrypt.Net.BCrypt.GenerateSalt();
        passwordHash = BCrypt.Net.BCrypt.HashPassword(registerDto.Password, passwordSalt);

        var insertQuery = @"
            INSERT INTO Users (Username, Email, Name, Passwordhash, Passwordsalt)
            VALUES (@Username, @Email, @Name, @Passwordhash, @Passwordsalt)";

        var userParameters = new User()
        {
            Username = registerDto.Username,
            Email = registerDto.Email,
            Name = registerDto.Name,
            Passwordhash = passwordHash,
            Passwordsalt = passwordSalt
        };

        var rowsAffected = await _dbConnection.ExecuteAsync(insertQuery, userParameters);

        if (rowsAffected > 0)
        {
            Member currentUser = await _userRepository.GetUserByEmailAsync(registerDto.Email);
            _redisService.SetUserSessionAsync(currentUser.UserId.ToString(), currentUser);
            var user = new UserDto()
            {
                Email = registerDto.Email,
                Token = await _tokenService.CreateToken(userParameters)
            };

            return user;
        }
        else
        {
            return null;
        }
    }

    public async Task<bool> IsEmailTaken(string? email)
    {
        var query = "SELECT 1 FROM Users WHERE Email = @Email";
        var count = await _dbConnection.ExecuteScalarAsync<int>(query, new { Email = email });
        return count > 0;
    }

    public async Task<bool> IsUsernameTaken(string? username)
    {
        var query = "SELECT 1 FROM Users WHERE Username = @Username";
        var count = await _dbConnection.ExecuteScalarAsync<int>(query, new { Username = username });
        return count > 0;
    }

    public async Task<UserDto> Login(LoginDto loginDto)
    {
        var query = "SELECT * FROM Users WHERE email = @Email";
        var user = await _dbConnection.QuerySingleOrDefaultAsync<User>(query, new { Email = loginDto.Email });

        if (user == null)
            return null;

        var verifyPasswordData = BCrypt.Net.BCrypt.Verify(loginDto.Password, user.Passwordhash, false);

        if (verifyPasswordData)
        {
            user.LastOnline = DateTime.UtcNow;
            var updateLastOnlineQuery = "UPDATE Users SET Last_Online = @LastOnline WHERE Email = @Email";
            await _dbConnection.ExecuteAsync(updateLastOnlineQuery, new { LastOnline = user.LastOnline, Email = loginDto.Email });
            
            Member currentUser = await _userRepository.GetUserByEmailAsync(loginDto.Email);
            _redisService.SetUserSessionAsync(currentUser.UserId.ToString(), currentUser);
            
            return new UserDto
            {
                Email = user.Email,
                Token = await _tokenService.CreateToken(user)
            };
        }
        else
        {
            return null;
        }
    }

    public async ValueTask DisposeAsync()
    {
        if (_dbConnection is IAsyncDisposable dbConnectionAsyncDisposable)
            await dbConnectionAsyncDisposable.DisposeAsync();
        else
            _dbConnection.Dispose();
    }
}