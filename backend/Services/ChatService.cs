﻿using System.Data;
using Dapper;
using OnlineChat.Interfaces;
using OnlineChat.Models;

namespace OnlineChat.Services;

public class ChatService: IChatService
{
    private readonly IUserRepository _userRepository;
    private readonly RedisService _redisService;
    private readonly IDbConnection _dbConnection;

    public ChatService(IDapperDbRepository dapperDbRepository, IUserRepository userRepository,
        RedisService redisAsync)
    {
        _userRepository = userRepository;
        _redisService = redisAsync;
        _dbConnection = dapperDbRepository.CreateConnection();
    }
    
    public async Task<int> GetChatIdByName(string chatName)
    {
        var query = "SELECT id FROM chats WHERE chatname = @chatName";
        var operationResult = _dbConnection.QueryFirstOrDefault<int>(query, new { chatName });
        return operationResult;
    }

    public Task<int?> GetChatIdByUserIds(List<int> userIds)
    {
        if (userIds.Count != 2)
            throw new ArgumentException("Exactly two user IDs are required.");

        var query = @"
        SELECT cu1.chat_id 
        FROM chat_users cu1
        JOIN chat_users cu2 ON cu1.chat_id = cu2.chat_id
        WHERE cu1.user_id = @UserId1 AND cu2.user_id = @UserId2";
        
        var chatId = _dbConnection.QueryFirstOrDefaultAsync<int?>(
            query, 
            new { UserId1 = userIds[0], UserId2 = userIds[1] }
        );

        return chatId ;
    }

    public async Task JoinChatAsync(NewChatRequestDto newChatRequestDto, string currentUserUsername)
    {
        int? currentUserId = await _userRepository.GetUserIdByUsernameAsync(currentUserUsername);
        int? contactId = newChatRequestDto.ContactId;

        string chatRoom = newChatRequestDto.ChatRoom;
        DateTime creationDate = DateTime.UtcNow;


        Console.WriteLine($"currentUserId: {currentUserId}, contactId: {contactId}, chatRoom: {chatRoom}");

        if (currentUserId == null || contactId == null)
            throw new InvalidOperationException("Invalid user or chat information.");

        using var connection = _dbConnection;
        connection.Open();

        using var transaction = connection.BeginTransaction();

        try
        {
            int? chatId = await GetChatIdByName(newChatRequestDto.ChatRoom);

            if (chatId == 0)
            {
                var insertChatQuery = @"
                INSERT INTO chats (chatName, createddate)
                VALUES (@chatRoom, @creationDate)
                RETURNING id;";

                chatId = await connection.ExecuteScalarAsync<int>(insertChatQuery,
                    new { chatRoom = newChatRequestDto.ChatRoom, creationDate }, transaction);

                var insertChatUsersQuery = @"
                INSERT INTO chat_users (chat_id, user_id)
                VALUES (@chatId, @contactId), (@chatId, @currentUserId);";

                await connection.ExecuteAsync(insertChatUsersQuery,
                    new { chatId, contactId, currentUserId }, transaction);

                transaction.Commit();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Transaction failed: {ex.Message}");
            transaction.Rollback();
            throw;
        }
    }

    public async Task<List<int>> GetChatsIdsByUserId(int userId)
    {
        var query = "SELECT chat_id FROM chat_users WHERE user_id = @userId";
        var operationResult = await _dbConnection.QueryAsync<int>(query, new { userId });
        List<int> ChatIdsPresentByUserId = operationResult.ToList();
        return ChatIdsPresentByUserId;
    }

    public async Task<Dictionary<int, List<int>>> GetUserIdsByChatIdsAsync(List<int> chatIds)
    {
        Dictionary<int, List<int>> chatsAndUsers = new Dictionary<int, List<int>>();
        foreach (var id in chatIds)
        {
            var query = "SELECT user_id FROM chat_users WHERE chat_id = @id";
            var operationResult = await _dbConnection.QueryAsync<int>(query, new {id});
            chatsAndUsers.Add(id, operationResult.ToList());
        }

        return chatsAndUsers;
    }

    public async Task<List<Member>> GetRecentChats(Dictionary<int, List<int>> chatAndUsers, int currentUserId)
    {
        List<Member> recentContactChats = new List<Member>();

        foreach (var chat in chatAndUsers)
        {
            List<int> userIds = chat.Value;
            
            userIds.Remove(currentUserId);

            if (userIds.Any())
            {
                var query = "SELECT userid, username FROM users WHERE userid = @userid";
                
                var operationResult = await _dbConnection.QueryAsync<Member>(query, new { userid = userIds[0] });
                
                recentContactChats.AddRange(operationResult);
            }
        }

        return recentContactChats;
    }

    public async Task AddUsersToChatAsync(int chatId, string userId)
    {
        string insertChatUserQuery = "INSERT INTO chat_users (chat_id, user_id) VALUES (@ChatId, @UserId)";
        var operationResult =
            await _dbConnection.ExecuteAsync(insertChatUserQuery, new { ChatId = chatId, UserId = userId });
    }
}