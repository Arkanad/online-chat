﻿using System.Data;
using Azure.Storage.Blobs;
using Dapper;
using OnlineChat.DTO;
using OnlineChat.Interfaces;
using OnlineChat.Models;

namespace OnlineChat.Services;

public class FileService: IFileService
{
    private readonly BlobServiceClient _blobServiceClient;
    private string _containerName;
    private IDbConnection _dbConnection;

    public FileService(BlobServiceClient blobServiceClient, IDapperDbRepository dapperDbRepository)
    {
        _blobServiceClient = blobServiceClient;
        _dbConnection = dapperDbRepository.CreateConnection();
    }

    public async Task<string> Upload(Image image, string containerName)
    {
        _containerName = containerName;
        var containerInstance = _blobServiceClient.GetBlobContainerClient(_containerName);
        
        string newFileName = $"{Guid.NewGuid().ToString()}_{image.ImageFile.FileName}";
        
        var blobInstance = containerInstance.GetBlobClient(newFileName);
        
        await blobInstance.UploadAsync(image.ImageFile.OpenReadStream());
        
        return blobInstance.Uri.ToString();
    }
    
    public async Task<Stream> Get(String name, string containerName)
    {
        var containerInstance = _blobServiceClient.GetBlobContainerClient(containerName);
        
        var blobInstance = containerInstance.GetBlobClient(name);

        var downloadContent = await blobInstance.DownloadAsync();

        return downloadContent.Value.Content;

    }

    public async Task SaveFileToDb(FileDTO fileDto)
    {
        if (fileDto == null)
        {
            throw new ArgumentNullException(nameof(fileDto), "FileDTO cannot be null");
        }

        var insertQuery = @"INSERT INTO file_messages (chat_id, sender_id, file_name, file_type, file_size, file_url, created_at) 
                        VALUES (@ChatId, @SenderId, @FileName, @FileType, @FileSize, @FileUrl, @CreatedAt)";

        using (var connection = _dbConnection)
        {
            try
            {
                connection.Open(); 

                var parameters = new 
                {
                    ChatId = fileDto.ChatId,                  // Assuming ChatId is a property of FileDTO
                    SenderId = fileDto.SenderId,              // Assuming SenderId is a property of FileDTO
                    FileName = fileDto.FileName,
                    FileType = fileDto.FileType,
                    FileSize = fileDto.FileSize,              // Assuming FileSize is a property of FileDTO
                    FileUrl = fileDto.FileUrl,
                    CreatedAt = DateTime.UtcNow               // Set the created_at timestamp to the current time
                };

                var rowsAffected = await connection.ExecuteAsync(insertQuery, parameters);
            
                if (rowsAffected == 0)
                {
                    throw new InvalidOperationException("No rows were inserted into the database.");
                }
            }
            catch (Exception ex)
            {
                // Handle general exceptions
                throw new Exception("An error occurred while saving the file.", ex);
            }
        }
    }



}