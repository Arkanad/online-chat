﻿using System.Collections;
using System.Data;
using Dapper;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using OnlineChat.DTO;
using OnlineChat.Interfaces;
using OnlineChat.Models;
using StackExchange.Redis;
using System.Linq;

namespace OnlineChat.Services;

public class MessagingService : IMessagingService
{
    private IDbConnection _dbConnection;
    private IUserRepository _userRepository;
    private RedisService _redisService;

    public MessagingService(IDapperDbRepository dapperDbRepository, IUserRepository userRepository,
        RedisService redisAsync)
    {
        _userRepository = userRepository;
        _redisService = redisAsync;
        _dbConnection = dapperDbRepository.CreateConnection();
    }

    public async Task<int> GetChatIdByName(string chatName)
    {
        var query = "SELECT id FROM chats WHERE chatname = @chatName";
        var operationResult = _dbConnection.QueryFirstOrDefault<int>(query, new { chatName });
        return operationResult;
    }

    public async Task<Dictionary<int, List<int>>> GetUserIdsByChatIdsAsync(List<int> chatIds)
    {
        Dictionary<int, List<int>> chatsAndUsers = new Dictionary<int, List<int>>();
        foreach (var id in chatIds)
        {
            var query = "SELECT user_id FROM chat_users WHERE chat_id = @id";
            var operationResult = await _dbConnection.QueryAsync<int>(query, new {id});
            chatsAndUsers.Add(id, operationResult.ToList());
        }

        return chatsAndUsers;
    }

    public async Task<List<int>> GetUserIdsOfChatByChatIdsAsync(IEnumerable<int> chatIds)
    {
        if (chatIds == null || !chatIds.Any())
        {
            return new List<int>();
        }

        var query = @"
    SELECT user_id
    FROM chat_users
    WHERE chat_id IN @ChatIds";

        var operationResult = await _dbConnection.QueryAsync<int>(query, new { ChatIds = chatIds });
        return operationResult.ToList();
    }


    public async Task<int> SendMessageAsync(MessageDto messageDto)
    {
        using var connection = _dbConnection;
        connection.Open();

        using var transaction = connection.BeginTransaction();

        try
        {
            var messageId = await connection.QuerySingleOrDefaultAsync<int>(
                "INSERT INTO messages (chat_Id, user_id, username, content, sent_at) " +
                "VALUES (@ChatId, @UserId, @Username ,@Content, @Sent_at)" +
                "RETURNING id",
                new
                {
                    ChatId = messageDto.ChatId,
                    UserId = messageDto.UserId,
                    Username = messageDto.Username,
                    Content = messageDto.Content,
                    Sent_at = messageDto.Sent_at
                },
                transaction
            );

            transaction.Commit();

            return messageId;
        }
        catch
        {
            transaction.Rollback();
            throw;
        }
    }

    public Task<IEnumerable<MessageDto>> LoadChatHistory(int chatId)
    {
        var query = @"
        SELECT content AS Content,
               sent_at AS Sent_at,
               username AS Username
        FROM messages
        WHERE chat_id = @chatId";
        
        var operationResult = _dbConnection.QueryAsync<MessageDto>(query, new { chatId });
        return operationResult;
    }


    public Task<Message> GetMessageByIdAsync(string messageId)
    {
        throw new NotImplementedException();
    }

    public Task DeleteMessage(int messageId)
    {
        throw new NotImplementedException();
    }

    public Task SelectMessage(int messageId)
    {
        throw new NotImplementedException();
    }

    public Task<Message> EditMessage(int messageId)
    {
        throw new NotImplementedException();
    }

    public async Task<int?> GetExistingChatAsync(int userId1, int userId2)
    {
        var query = @"
            SELECT cp1.ChatId
            FROM ChatParticipants cp1
            JOIN ChatParticipants cp2 ON cp1.ChatId = cp2.ChatId
            WHERE cp1.UserId = @UserId1 AND cp2.UserId = @UserId2
            LIMIT 1";
        return await _dbConnection.QuerySingleOrDefaultAsync<int?>(query, new { UserId1 = userId1, UserId2 = userId2 });
    }

    public async Task<IEnumerable<int>> GetChatParticipantsAsync(int chatId)
    {
        var query = "SELECT UserId FROM ChatParticipants WHERE ChatId = @ChatId";
        return await _dbConnection.QueryAsync<int>(query, new { ChatId = chatId });
    }

 
}