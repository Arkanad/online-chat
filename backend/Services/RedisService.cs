﻿using Newtonsoft.Json;
using OnlineChat.DTO;
using OnlineChat.Models;
using StackExchange.Redis;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace OnlineChat.Services;

public class RedisService
{
    private readonly IConnectionMultiplexer _connectionMultiplexer;

    public RedisService(IConnectionMultiplexer connectionMultiplexer)
    {
        _connectionMultiplexer = connectionMultiplexer;
    }

    public async Task SetUserSessionAsync(string sessionId, Member member)
    {
        if (string.IsNullOrEmpty(sessionId))
            throw new ArgumentNullException(nameof(sessionId));

        if (member == null)
            throw new ArgumentNullException(nameof(member));

        try
        {
            var cache = _connectionMultiplexer.GetDatabase(); // Renamed for clarity

            var sessionData = new HashEntry[]
            {
                new HashEntry("name", member.Name ?? string.Empty),
                new HashEntry("username", member.Username ?? string.Empty),
                new HashEntry("email", member.Email ?? string.Empty),
            };
            
            await cache.HashSetAsync(sessionId, sessionData);
            
            var cacheExpiration = TimeSpan.FromHours(24);
            await cache.KeyExpireAsync(sessionId, cacheExpiration);
        }
        catch (RedisException redisEx)
        {
            Console.WriteLine($"A Redis error occurred while setting the user session: {redisEx.Message}");
            throw;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while setting the user session: {ex.Message}");
            throw;
        }
    }
    
    public async Task<Member> GetUserSessionAsync(string sessionId)
    {
        if (string.IsNullOrEmpty(sessionId))
            throw new ArgumentNullException(nameof(sessionId));

        try
        {
            var cache = _connectionMultiplexer.GetDatabase(); // The same method used to get the Redis connection

            // Get all the fields and values in the hash stored at sessionId
            var sessionData = await cache.HashGetAllAsync(sessionId);

            if (sessionData.Length == 0)
            {
                // No data found for the session
                return null;
            }

            // Map the retrieved hash entries to a MemberDto object
            var member = new Member
            {
                Name = sessionData.FirstOrDefault(x => x.Name == "name").Value,
                Username = sessionData.FirstOrDefault(x => x.Name == "username").Value,
                Email = sessionData.FirstOrDefault(x => x.Name == "email").Value
            };

            return member;
        }
        catch (RedisException redisEx)
        {
            Console.WriteLine($"A Redis error occurred while retrieving the user session: {redisEx.Message}");
            throw;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while retrieving the user session: {ex.Message}");
            throw;
        }
    }

    
    public async Task<List<MessageDto>> GetMessagesAsync(string chatRoom)
    {
        var db = _connectionMultiplexer.GetDatabase();
        var chatKey = $"chat:{chatRoom}";

        var messages = await db.ListRangeAsync(chatKey);
        return messages.Select(msg => JsonConvert.DeserializeObject<MessageDto>(msg)).ToList();
    }

    public async Task SetUserOnlineAsync(int userId)
    {
        var db = _connectionMultiplexer.GetDatabase();
        await db.StringSetAsync($"user:{userId}:status", "online");
    }

    public async Task SetUserOfflineAsync(int userId)
    {
        var db = _connectionMultiplexer.GetDatabase();
        await db.StringSetAsync($"user:{userId}:status", "offline");
    }

    public async Task<bool> isUserOnline(int userId)
    {
        var db = _connectionMultiplexer.GetDatabase();
        var result = await db.StringGetAsync($"user:{userId}:status");
        if (result == "online")
            return true;
        else
            return false;
    }

    public async Task<bool> IsUserOnlineAsync(int userId)
    {
        var db = _connectionMultiplexer.GetDatabase();
        var status = await db.StringGetAsync($"user:{userId}:status");
        return status == "online";
    }

    public async Task<int> GetCurrentChatIdAsync(int currentUserId)
    {
        var cache = _connectionMultiplexer.GetDatabase();
        var chatIdString = await cache.StringGetAsync($"Chat_{currentUserId}");
        
        if (int.TryParse(chatIdString, out var chatId))
        {
            return chatId;
        }

        throw new InvalidOperationException("Chat id not found for the given connecton ID.");
    }

    public async Task SetCurrentChatIdAsync(int currentUserId, int chatId)
    {
        var cache = _connectionMultiplexer.GetDatabase();
        var chatIdString = await cache.StringSetAsync($"Chat_{currentUserId}",chatId);
    }
 
}

