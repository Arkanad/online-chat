﻿using System.Data;
using Dapper;
using OnlineChat.Interfaces;
using OnlineChat.Models;

public class SessionService : ISessionService
{  
    private readonly IDbConnection _dbConnection;

    public SessionService(IDapperDbRepository dapperDbRepository)
    {
        _dbConnection = dapperDbRepository.CreateConnection();
    }

    public async Task AddSessionAsync(string username, string chatRoom, string connectionId)
    {
        var query = @"
            INSERT INTO Sessions (username, chatname, connectionid)
            VALUES (@username, @chatRoom, @connectionId)";
        await _dbConnection.ExecuteAsync(query, new { username = username, chatRoom = chatRoom, connectionid = connectionId});
    }

    public async Task RemoveSessionAsync(string connectionId)
    {
        var query = "DELETE FROM Sessions WHERE ConnectionId = @ConnectionId";
        await _dbConnection.ExecuteAsync(query, new { ConnectionId = connectionId });
    }

    public async Task<int?> GetUserIdByConnectionIdAsync(string connectionId)
    {
        var query = "SELECT UserId FROM Sessions WHERE ConnectionId = @ConnectionId";
        return await _dbConnection.QueryFirstOrDefaultAsync<int?>(query, new { ConnectionId = connectionId });
    }

    public async Task<UserConnection> GetSessionByConnectionIdAsync(string contextConnectionId)
    {
        var query = @"
        SELECT 
            username AS Username, 
            chatname AS ChatName,
            connectionid as contextConnectionId
        FROM Sessions 
        WHERE connectionid = @connectionId";
        return await _dbConnection.QueryFirstOrDefaultAsync<UserConnection>(query, new { connectionid = contextConnectionId});
    }

    public async Task<UserConnection> GetSessionByContextConnectionId(string contextConnectionId)
    {
        var query = @"SELECT 
            username AS Username, 
            chatname AS ChatName,
            connectionid as contextConnectionIdFROM Sessions WHERE connectionid = @contextConnectionId";
        return _dbConnection.QueryFirstOrDefault<UserConnection>(query, new { connectionid = contextConnectionId });
    }

    public async Task<IEnumerable<int>> GetOnlineUserIdsAsync()
    {
        var query = "SELECT DISTINCT UserId FROM Sessions";
        return await _dbConnection.QueryAsync<int>(query);
    }

    public async Task<bool> isSessionExist(string contextConnectionId)
    {
        var query = "SELECT COUNT(1) FROM Sessions WHERE connectionid = @connectionId";
        int count = await _dbConnection.ExecuteScalarAsync<int>(query, new { connectionId = contextConnectionId });
        return count > 0;
    }

    

}
