import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Home } from './pages/Home';
import { Login } from './pages/Login';
import Registration from './pages/Registration';
import Layout from './layout/Layout';
import Profile from './pages/Profile';
import GuardedRoute from './utils/GuardedRoute';
import ContactsPage from './pages/ContactsPage';
import ChattingPage from './pages/ChattingPage';
import AboutPage from './pages/AboutPage';
import ProtectedRoutes from './utils/GuardedRoute';
import { useState } from 'react';

function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={
            <Login />
          } />
          <Route path="/register" element={<Registration />} />
          <Route element={<ProtectedRoutes />}>
            <Route path="/" element={<Layout><Home /></Layout>} />
            <Route
              path="/recentChats" element={<Layout><ChattingPage /></Layout>} />
            <Route
              path="/chatting" element={<Layout><ContactsPage /></Layout>} />
            <Route
              path="/about" element={<Layout><AboutPage /></Layout>} />
            <Route
              path="/profile" element={<Layout><Profile /></Layout>} />
          </Route>
        </Routes>
      </BrowserRouter>
  );
}

export default App;
