import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:5039/',
  withCredentials: true,  // Ensures cookies, including HttpOnly ones, are sent
  headers: {
    'Content-Type': 'application/json',
},
});

export default api;
