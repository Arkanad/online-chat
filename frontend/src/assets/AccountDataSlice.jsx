import { createSlice } from '@reduxjs/toolkit';

// Updated initialState with username and id properties
const initialState = {
  username: "",
  id: null,  // Initialize id as null or a suitable default value
};

const accountDataSlice = createSlice({
  name: 'accountData',
  initialState,
  reducers: {
    setAccountUsername: (state, action) => {
      state.username = action.payload;
    },
    setAccountId: (state, action) => {
      state.id = action.payload;
    },
  },
});

export const { setAccountUsername, setAccountId } = accountDataSlice.actions;

// Updated selectors to directly return the properties
export const selectAccountUsername = (state) => state.accountData.username;
export const selectAccountId = (state) => state.accountData.id;

export default accountDataSlice.reducer;
