// authSlice.js
import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isAuthenticated: false,
    user: null,
  },
  reducers: {
    setAuthenticated(state, action) {
      state.isAuthenticated = true;
      state.user = action.payload;
    },
    setUnauthenticated(state) {
      state.isAuthenticated = false;
      state.user = null;
    },
    logout(state) {
      state.isAuthenticated = false;
      state.user = null;
    },
  },
});

export const { setAuthenticated, setUnauthenticated, logout } = authSlice.actions;
export default authSlice.reducer;
