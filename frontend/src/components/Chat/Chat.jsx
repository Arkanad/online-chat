import React, { useState, useEffect, useRef } from "react";
import { VStack } from "@chakra-ui/react";
import { useSelector } from "react-redux";
import api from "../../api/api";
import ChatHeader from "./ChatHeader";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import FileUploadModal from "./FileUploadModal";
import { useDisclosure } from "@chakra-ui/react";
import { selectAccountUsername } from "../../assets/AccountDataSlice";
import { getConnection } from "../../services/chatService";

const Chat = ({ closeChat, contactName }) => {
    const connection = getConnection();
    const [message, setMessage] = useState("");
    const [messages, setMessages] = useState([]);
    const [selectedFile, setSelectedFile] = useState(null);
    const chatEndRef = useRef(null);
    const currentUserUsername = useSelector(selectAccountUsername);
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [isSelectedClick, SetIsSelectedClick] = useState(false);

    useEffect(() => {
        const onReceiveMessage = (username, message, sentAt) => {
            const parsedTime = sentAt ? new Date(sentAt) : null;
            setMessages(prevMessages => [...prevMessages, { username, message, sentAt: parsedTime }]);
        };

        const onLoadChatHistory = (loadedMessages) => {
            const formattedMessages = loadedMessages.map(msg => ({
                username: msg.username,
                message: msg.content,
                sentAt: msg.sent_at
            }));
            setMessages(formattedMessages);
        };

        connection.on("ReceiveMessage", onReceiveMessage);
        connection.on("LoadChatHistory", onLoadChatHistory);

        return () => {
            connection.off("ReceiveMessage", onReceiveMessage);
            connection.off("LoadChatHistory", onLoadChatHistory);
        };
    }, [connection]);

    useEffect(() => {
        if (chatEndRef.current) {
            chatEndRef.current.scrollIntoView({ behavior: "smooth" });
        }
    }, [messages]);

    const handleFileChange = (event) => {
        const files = event.target.files;
        if (files.length === 0) return console.error("No files selected.");
        setSelectedFile(files[0]);
        onOpen();
    };

    const handleFileUpload = async () => {
        if (!selectedFile) return;
        const formData = new FormData();
        formData.append("ImageFile", selectedFile);

        try {
            const response = await api.post("/api/messaging/upload", formData, {
                headers: { "Content-Type": "multipart/form-data" }
            });

            if (response.status === 200) {
                const { fileUrl, fileType } = response.data;
                console.log("File uploaded successfully", fileUrl, fileType);
                if (fileType.startsWith("image/")) {
                    sendMessage(fileUrl);
                } else {
                    sendMessage(`File uploaded: ${fileUrl}`);
                }
            } else {
                console.error("Error uploading file:", response.data);
            }
        } catch (error) {
            console.error("Error during file upload:", error);
        } finally {
            setSelectedFile(null);
            onClose();
        }
    };

    const sendMessage = async (message) => {
        if (!connection || message.trim() === "") return;

        try {
            await connection.invoke("SendMessage", message);
            console.log("Message sent successfully.");
            setMessage("");
        } catch (error) {
            console.error("Error sending message:", error);
        }
    };

    return (
        <VStack height="100vh" width="100%" overflow="hidden" backgroundColor={"#18191d"}>
            <ChatHeader closeChat={closeChat} contactName={contactName} />
            <MessageList 
                messages={messages} 
                currentUserUsername={currentUserUsername} 
                SetIsSelectedClick={SetIsSelectedClick} 
                chatEndRef={chatEndRef} 
            />
            <MessageInput 
                sendMessage={sendMessage} 
                handleFileChange={handleFileChange} 
                contactName={contactName} 
                message={message} 
                setMessage={setMessage} 
            />
            <FileUploadModal 
                isOpen={isOpen} 
                onClose={onClose} 
                selectedFile={selectedFile} 
                handleFileUpload={handleFileUpload} 
            />
        </VStack>
    );
};

export default Chat;
