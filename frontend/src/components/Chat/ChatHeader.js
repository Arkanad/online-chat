import React from 'react';
import { Flex, Text, IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";

const ChatHeader = ({ closeChat, contactName }) => {
    return (
        <Flex justify="space-between" width="100%" margin={"auto"} padding={2} borderBottom="1px" borderColor="white"  backgroundColor="#18191d">
            <Text fontSize={25}>{`Chat with ${contactName}`}</Text>
            <IconButton aria-label="Close chat" icon={<CloseIcon />} onClick={closeChat} />
        </Flex>
    );
};

export default ChatHeader;
