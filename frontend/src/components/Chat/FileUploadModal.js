import React from 'react';
import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter, Button, Image } from "@chakra-ui/react";

const FileUploadModal = ({ isOpen, onClose, selectedFile, handleFileUpload }) => {
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Confirm File Upload</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    {selectedFile && <Image src={URL.createObjectURL(selectedFile)} alt="Preview" maxW="100%" />}
                </ModalBody>
                <ModalFooter>
                    <Button colorScheme="blue" mr={3} onClick={handleFileUpload}>
                        Confirm
                    </Button>
                    <Button variant="ghost" onClick={onClose}>Cancel</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
};

export default FileUploadModal;
