import React, { useState, useEffect } from 'react';
import { Box, Image, Text, HStack } from "@chakra-ui/react";

// ContextMenu component for the right-click menu
const ContextMenu = ({ top, left, children }) => {
    return (
        <Box
            position="absolute"
            top={top}
            left={left}
            bg="white"
            p={2}
            boxShadow="md"
            borderRadius="md"
            zIndex={9999}  // Ensure it appears on top
        >
            {children}
        </Box>
    );
};

const MessageBubble = React.memo(({ messageInfo, isOwnMessage, SetIsSelectedClick }) => {
    const ownMessageStyle = {
        alignSelf: 'flex-end',
        background: "#2b5378",
        color: 'white',
        borderRadius: '20px 20px 0px 20px',
        boxShadow: '0 4px 10px rgba(0, 0, 0, 0.5)',
    };

    const otherMessageStyle = {
        alignSelf: 'flex-start',
        background: "#182533",
        color: 'white',
        borderRadius: '20px 20px 20px 0px',
        boxShadow: '0 4px 10px rgba(0, 0, 0, 0.1)',
    };

    const formattedTime = messageInfo.sentAt
        ? new Date(messageInfo.sentAt).toLocaleTimeString([], {
            hour: '2-digit',
            minute: '2-digit',
        })
        : "Unknown time";

    const isImageMessage = /\.(jpeg|jpg|gif|png|bmp|svg)(\?.*)?$/i.test(messageInfo.message);

    const [clicked, setClicked] = useState(false);
    const [points, setPoints] = useState({ x: 0, y: 0 });

    // Handle clicking outside the context menu to close it
    useEffect(() => {
        const handleClickOutside = () => setClicked(false);
        if (clicked) {
            document.addEventListener("click", handleClickOutside);
        }
        return () => document.removeEventListener("click", handleClickOutside);
    }, [clicked]);

    // Handle right-click event
    const handleRightClick = (e) => {
        e.preventDefault();
        setPoints({ x: e.pageX, y: e.pageY });
        setClicked(true);
        SetIsSelectedClick(true);
    };

    return (
        <HStack w="100%" m="auto" paddingTop={2} paddingLeft={3} paddingRight={3} justifyContent={isOwnMessage ? 'flex-end' : 'flex-start'}>
            <Box
                maxW="75%"
                p={3}
                style={isOwnMessage ? ownMessageStyle : otherMessageStyle}
                onContextMenu={handleRightClick}
            >
                {isImageMessage ? (
                    <Image src={messageInfo.message} alt="Uploaded file" width="150px" />
                ) : (
                    <Text>
                        {messageInfo.message.startsWith("File uploaded: ") ? (
                            <a href={messageInfo.message.replace("File uploaded: ", "")} target="_blank" rel="noopener noreferrer">
                                {messageInfo.message}
                            </a>
                        ) : (
                            messageInfo.message
                        )}
                    </Text>
                )}
                <Text fontSize="xs" textAlign="right" mt={1} color="gray.400">
                    {formattedTime}
                </Text>

                {/* Render the context menu on right-click */}
                {clicked && (
                    <ContextMenu top={points.y} left={points.x}>
                        <ul>
                            {isOwnMessage ? <li>Edit</li> : null}
                            <li>Copy</li>
                            <li>Delete</li>
                        </ul>
                    </ContextMenu>
                )}
            </Box>
        </HStack>
    );
});

export default MessageBubble;
