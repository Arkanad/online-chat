import React, { useRef, useState } from 'react';
import { Flex, Button, Input } from "@chakra-ui/react";

const MessageInput = ({ sendMessage, handleFileChange, contactName, message, setMessage }) => {
    const inputFile = useRef(null);

    const handleKeyDown = (event) => {
        if (event.key === "Enter" && message.trim()) {
            sendMessage(message);
        }
    };

    return (
        <Flex p={5} width="100%" alignItems="center" flexShrink={0}>
            <Button onClick={() => inputFile.current.click()}>Send File</Button>
            <Input
                type="text"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                placeholder={`Enter message to ${contactName}`}
                onKeyDown={handleKeyDown}
                ml={2}
            />
            <Button onClick={() => sendMessage(message)} ml={2}>Send</Button>
            <input
                type="file"
                ref={inputFile}
                style={{ display: "none" }}
                onChange={handleFileChange}
            />
        </Flex>
    );
};

export default MessageInput;
