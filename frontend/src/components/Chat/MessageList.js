import React from 'react';
import { Box } from "@chakra-ui/react";
import MessageBubble from './MessageBubble';

const MessageList = ({ messages, currentUserUsername, SetIsSelectedClick, chatEndRef }) => {
    return (
        <Box flex="1" width="100%" overflowY="auto">
            {messages.map((msg, index) => (
                <div key={index} >
                    <MessageBubble
                        messageInfo={msg}
                        isOwnMessage={currentUserUsername === msg.username}
                        SetIsSelectedClick={SetIsSelectedClick}
                    />
                </div>
            ))}
            <div ref={chatEndRef} />
        </Box>
    );
};

export default MessageList;
