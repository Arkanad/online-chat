import { useState, useEffect } from "react";
import { Flex, Box, Text, Heading, Stack, Avatar } from "@chakra-ui/react";
import api from "../api/api";
import { useSelector } from "react-redux";
import { selectAccountId, selectAccountUsername } from "../assets/AccountDataSlice";
import UserStatus from "./UserStatus";

const ContactsList = ({ apiUrl, heading, onJoinChat }) => {
  const accountId = useSelector(selectAccountId);
  const accountUsername = useSelector(selectAccountUsername);
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    const fetchContacts = async () => {
      try {
        const response = await api.get(apiUrl);
        const users = response.data;
        const contactsData = users.filter(user => user.username !== accountUsername);
        setContacts(contactsData);
      } catch (error) {
        console.error('Error fetching contacts:', error);
      }
    };

    fetchContacts();
  }, [apiUrl, accountUsername]);

  const generateChatName = (userId, contactId) => {
    const [firstId, secondId] = [userId, contactId].sort();
    return `${firstId}-${secondId}`;
  };
  
  return (
    <Flex direction="column" h="100%" w="100%">
      <Heading mb={4} pl="4" pt="4">{heading}</Heading>
      <Box flex="1" align="center" justify="center" pt="10px">
        <Stack spacing={2}>
          {contacts.map((contact, index) => (
            <Box
              key={`${contact.userId}_${index}`}
              p={4}
              borderWidth="1px"
              borderRadius="lg"
              w="full"
              cursor="pointer"
              onClick={() => {
                const chatRoomName = generateChatName(accountId, contact.userId);
                onJoinChat(contact.userId, contact.username, chatRoomName); 
              }}
            >
              <Flex align="center">
                
                <Avatar mr={4} />
                
                <Box flex="1" align="left">
                  <Text fontWeight="bold">{contact.username}</Text>
                </Box>

                <UserStatus userId={contact.userId} />
              </Flex>
            </Box>
          ))}
        </Stack>
      </Box>
    </Flex>
  );
};

export default ContactsList;
