const handleInputChange = useCallback((event) => {
    handleChange(event, formId);
 }, [formId, handleChange]);
 <input handleChange={handleInputChange} />
 export default React.memo(Input)