import React, { useState } from 'react';
import { FaUser, FaCoffee, FaGrinWink, FaBars, FaAddressBook, FaHome, FaSignOutAlt } from 'react-icons/fa';
import { Flex, Box, useColorModeValue } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import api from '../api/api';
import { selectAccountUsername } from '../assets/AccountDataSlice';
import { useSelector } from 'react-redux';
import { Refresh } from '@mui/icons-material';

const Sidebar = ({ isCollapsed, toggleSidebar }) => {
  const navigate = useNavigate();
  const sidebarBg = "#282e33"; 
  const sidebarTextColor = useColorModeValue('gray.800', 'white');
  const sidebarHighlight = "#18191d"
  const CurrentUserUsername = useSelector(selectAccountUsername);

  const routeChange = (route) =>{ 
    navigate(route);
  }

  const handleLogout = async () => {
    try {
      await api.post('api/account/logout', CurrentUserUsername);
      window.location.reload();
      navigate('/');
    } catch (error) {
      console.error("Logout failed", error);
    }
  }

  return (
    <>
      <Flex direction="column" bg={sidebarBg} h="100%" w={isCollapsed ? '64px' : '250px'} transition="width 0.3s ease" justifyContent="space-between">
        <Flex
          p="6"
          bg="#18191d"
          color="white"
          alignItems="center"
          justifyContent="space-between"
          overflow="hidden"
        >
          <span className={`text-xl font-semibold ${isCollapsed ? 'hidden' : 'block'}`}>
            Snower
          </span>
          <FaBars
            style={{ cursor: 'pointer' }}
            onClick={toggleSidebar}
          />
        </Flex>

 
        <Flex mt="4" px="2" direction="column" justifyContent="flex-start" flex="1">
          <Flex
            as="button"
            alignItems="center"
            p="3"
            w="full"
            align="top"
            bg={sidebarBg}
            color={sidebarTextColor}
            _hover={{ bg: sidebarHighlight }}
            borderRadius="md" 
            onClick={() => routeChange('/')}
          >
            <FaHome style={{ fontSize: 24 }} />
            <span className={`ml-3 ${isCollapsed ? 'hidden' : 'block'}`}>Home</span>
          </Flex>

          <Flex
            as="button"
            d="flex"
            alignItems="center"
            p="3"
            w="full"
            textAlign="left"
            bg={sidebarBg}
            color={sidebarTextColor}
            _hover={{ bg: sidebarHighlight }}
            onClick={() => routeChange('/recentchats')}
            borderRadius="md"
          >
            <FaCoffee style={{ fontSize: 24 }} />
            <span className={`ml-3 ${isCollapsed ? 'hidden' : 'block'}`}>Recent chats</span>
          </Flex>

          <Flex
            as="button"
            d="flex"
            alignItems="center"
            p="3"
            w="full"
            textAlign="left"
            bg={sidebarBg}
            color={sidebarTextColor}
            _hover={{ bg: sidebarHighlight }}
            borderRadius="md" 
            onClick={() => routeChange('/chatting')}
          >
            <FaAddressBook style={{ fontSize: 24 }} />
            <span className={`ml-3 ${isCollapsed ? 'hidden' : 'block'}`}>Contacts</span>
          </Flex>

          <Flex
            as="button"
            d="flex"
            alignItems="center"
            p="3"
            w="full"
            textAlign="left"
            bg={sidebarBg}
            color={sidebarTextColor}
            _hover={{ bg: sidebarHighlight }}
            borderRadius="md"
            onClick={() => routeChange('/about')}
          >
            <FaGrinWink style={{ fontSize: 24 }} />
            <span className={`ml-3 ${isCollapsed ? 'hidden' : 'block'}`}>About</span>
          </Flex>
        </Flex>

        <Flex
          verticalAlign="center"
          direction="column"
          bottom="0"
          w="full"
          mb="2"
          p="2"
        >
          <Flex
            as="button"
            d="flex"
            alignItems="center"
            p="3"
            w="full"
            pb="4"
            textAlign="left"
            bg={sidebarBg}
            color={sidebarTextColor}
            _hover={{ bg: sidebarHighlight }}
            borderRadius="md"
            onClick={() => routeChange('/profile')}
          >
            <FaUser style={{ fontSize: 24 }} />
            <span className={`ml-3 ${isCollapsed ? 'hidden' : 'block'}`}>{CurrentUserUsername}</span>
          </Flex>
          <Flex

            as="button"
            d="flex"
            alignItems="center"
            p="3"
            w="full"
            textAlign="left"
            bg={sidebarBg}
            color={sidebarTextColor}
            _hover={{ bg: sidebarHighlight }}
            borderRadius="md"
            onClick={() => handleLogout()}
          >
            <FaSignOutAlt style={{ fontSize: 22 }} />
            <span className={`ml-5 ${isCollapsed ? 'hidden' : 'block'}`}>Log out</span>
          </Flex>
        </Flex>

        
      </Flex>
      
    </>
  );
};

export default Sidebar;
