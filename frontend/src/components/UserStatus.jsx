import { useEffect, useState } from 'react';
import axios from 'axios';
import api from '../api/api';

const UserStatus = ({ userId }) => {
  const [status, setStatus] = useState(false);

  useEffect(() => {
    const fetchStatus = async () => {
      try {
        const response = await api.get(`/api/users/GetUserOnlineStatus?userId=${userId}`);
        setStatus(response.data);
      } catch (error) {
        console.error('Error fetching user status', error);
      }
    };
    

    fetchStatus();

    const intervalId = setInterval(fetchStatus, 4500);
    return () => clearInterval(intervalId);
  }, [userId]);

  return <span>{status == true ? '🟢 Online' : '🔴 Offline'}</span>;
};

export default UserStatus;
