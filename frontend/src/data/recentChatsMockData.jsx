export function RecentChatsMockData() {
    const MockData = [
        {
            id: 1,
            name: 'Alice',
            latestMessage: 'Hey, how are you?',
            avatarUrl: 'https://bit.ly/dan-abramov',
        },
        {
            id: 2,
            name: 'Group: Project Team',
            latestMessage: 'Meeting at 3 PM.',
            avatarUrl: 'https://bit.ly/prosper-baba',
        },
        {
            id: 3,
            name: 'Bob',
            latestMessage: 'See you tomorrow!',
            avatarUrl: 'https://bit.ly/ryan-florence',
        },
        {
            id: 4,
            name: 'Group: Family',
            latestMessage: 'Dinner at 7 PM.',
            avatarUrl: 'https://bit.ly/kent-c-dodds',
        },
        {
            id: 5,
            name: 'Charlie',
            latestMessage: 'Can you send me the files?',
            avatarUrl: 'https://bit.ly/code-beast',
        }
    ]
    return MockData
}

