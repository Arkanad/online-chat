import React, { useState } from 'react';
import Sidebar from '../components/Sidebar';
import { Flex } from '@chakra-ui/react';

const Layout = ({ children }) => {
  const [isCollapsed, setIsCollapsed] = useState(false);

  const toggleSidebar = () => {
    setIsCollapsed(!isCollapsed);
  };

  return (
    <Flex direction="row" height="100%" overflowX="hidden" position="fill">
      <Flex
        position="fixed"
        direction="column"
        boxShadow="md"
        height="full"
        width={isCollapsed ? "5%" : "20%"} // 5% when collapsed, 20% when expanded
        transition="width 0.3s ease"
      >
        <Sidebar isCollapsed={isCollapsed} toggleSidebar={toggleSidebar} />
      </Flex>


      <Flex
        pl={[2, 4]} // Adjust padding based on screen size
        ml={isCollapsed ? ["10%", "64px"] : ["15%", "250px"]}
        direction="column"
        width={isCollapsed ? ["90%", "calc(100% - 64px)"] : ["85%", "calc(100% - 250px)"]}
        transition="margin-left 0.3s ease"
        backgroundColor="#18191d"
        height="100%"
      >
        {children}
      </Flex>

    </Flex>
  );
};

export default Layout;
