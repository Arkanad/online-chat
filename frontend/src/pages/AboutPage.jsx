import React from 'react';
import { Box, Text, Image, Button, Flex } from '@chakra-ui/react';

const AboutPage = () => {
  const handleSurprise = () => {
    alert('Surprise! 🦄✨ You’re awesome!');
  };

  return (
    <Flex direction="column" align="center" justify="center" h="100vh" p={8}>
      <Box
        textAlign="center"
        p={6}
        borderWidth="1px"
        borderRadius="lg"
        boxShadow="lg"
        maxW="600px"
      >
        <Image
          src="https://media.giphy.com/media/3o6ZtpxSZbQRRnwCKQ/giphy.gif"
          alt="Funny gif"
          borderRadius="full"
          boxSize="150px"
          mx="auto"
          mb={4}
        />
        <Text fontSize="2xl" fontWeight="bold" mb={2} color="white">
          About Us: We’re More Than Just Code!
        </Text>
        <Text fontSize="lg" color="gray.300" mb={4}>
          Here at <strong>FunTech Inc.</strong>, we believe in two things: <br />
          1. Writing clean, efficient code. <br />
          2. Finding the nearest snack after writing clean, efficient code. 🍕
        </Text>
        <Text fontSize="md" color="gray.400" mb={4}>
          In between debugging and deploying, you’ll often find us deep in thought, staring at code like it’s a modern
          art piece we "almost" understand. <br />
          When we’re not pushing commits, we’re probably pushing the limits of how many coffee cups we can stack. ☕️
        </Text>
        <Text fontSize="md" color="gray.400" mb={4}>
          Need help with your code? We’ll be there. Need someone to laugh at your coding jokes? Oh, we’re there too.
          Just don’t ask us why the chicken crossed the road—we’re still debugging that one. 🐔💻
        </Text>
        <Button onClick={handleSurprise} colorScheme="teal" mt={4}>
          Click Here for a Surprise! 🎉
        </Button>
        <Text fontSize="sm" color="gray.500" mt={2}>
          (Warning: This button is 100% fun and 0% productive.)
        </Text>
      </Box>
    </Flex>
  );
};

export default AboutPage;
