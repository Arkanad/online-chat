import React, { useEffect, useState } from 'react';
import { Flex } from '@chakra-ui/react';
import { useSelector } from 'react-redux';
import { selectAccountId, selectAccountUsername } from '../assets/AccountDataSlice';
import { joinChat } from '../services/chatService';
import ContactsList from '../components/ContactsList';
import api from '../api/api';
import Chat from '../components/Chat/Chat';
import { getConnection, closeChat } from '../services/chatService';

const ChattingPage = () => {
  const accountUsername = useSelector(selectAccountUsername);
  const recentChatsUrl = `/api/chat/GetAllChats?currentUserUsername=${accountUsername}`
  const [messages, setMessages] = useState([]);
  const [chats, setChats] = useState([]);
  const [chatRoom, setChatRoom] = useState("");
  const [contactName, setContactName] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [contactId, setContactId] = useState(0);

  const connection = getConnection();

  useEffect(() => {
    const fetchChats = async () => {
      try {
        const response = await api.get(recentChatsUrl);
        setChats(response.data);
      } catch (error) {
        setError('Failed to fetch chats');
      } finally {
        setLoading(false);
      }
    };

    fetchChats();
  }, [accountUsername]);

  const sendMessage = async (message) => {
    if (!connection) return;

    try {
      await connection.invoke("SendMessage", message);
      console.log("Message sent successfully.");
    } catch (error) {
      console.error("Error sending message:", error);
    }
  };

  const handleCloseChat = async () => {
    await closeChat();
    setMessages([]);
    setContactName("");
    setChatRoom("");
  };

  const handleJoinChat = async (selectedContactId, selectedContactName, chatRoomName) => {
    setContactName(selectedContactName);
    setContactId(selectedContactId);
    setChatRoom(chatRoomName);

    if (!connection) {
      try {
        await joinChat(selectedContactId, selectedContactName, chatRoomName);
      } catch (error) {
        console.error("Error joining chat:", error);
      }
    }
  };

  if (loading) return <div>Loading...</div>;
  if (error) return <div>{error}</div>;

  return (
    <Flex h="100vh" w="100%">
      {chatRoom ? (
        <Chat
          messages={messages}
          sendMessage={sendMessage}
          contactName={contactName}
          closeChat={handleCloseChat}
          style={{ flex: 1 }}
        />
      ) : (
        <ContactsList
          onJoinChat={handleJoinChat}
          apiUrl={recentChatsUrl}
          heading="Recent Chats"  
          style={{ flex: 1 }}
        />
      )}
    </Flex>
  );
};

export default ChattingPage;
