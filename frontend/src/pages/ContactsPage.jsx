import { useState, useEffect } from "react";
import Chat from "../components/Chat/Chat";
import ContactsList from "../components/ContactsList";
import { useSelector } from "react-redux";
import { Flex } from "@chakra-ui/react";
import { joinChat, closeChat, getConnection } from "../services/chatService";
import { selectAccountId, selectAccountUsername } from "../assets/AccountDataSlice";

const ContactsPage = () => {
  const [messages, setMessages] = useState([]);
  const [chatRoom, setChatRoom] = useState("");
  const [contactName, setContactName] = useState("");
  const [contactId, setContactId] = useState(null);
  const currentUserUsername = useSelector(selectAccountUsername);
  const currentUserId = useSelector(selectAccountId);
  const connection = getConnection();

  const handleJoinChat = async (selectedContactId, selectedContactName, chatRoomName) => {
    setContactName(selectedContactName);
    setContactId(selectedContactId);
    setChatRoom(chatRoomName);

    if (!connection) {
      try {
        await joinChat(selectedContactId, currentUserUsername, chatRoomName);
      } catch (error) {
        console.error("Error joining chat:", error);
      }
    }
  };

  const handleCloseChat = async () => {
    await closeChat();
    setMessages([]);
    setContactName("");
    setChatRoom("");
  };

  const sendMessage = async (message) => {
    if (!connection) return;

    try {
      await connection.invoke("SendMessage", message);
      console.log("Message sent successfully.");
    } catch (error) {
      console.error("Error sending message:", error);
    }
  };

  return (
    <Flex h="100%" w="100%">
      {chatRoom ? (
        <Chat
          messages={messages}
          sendMessage={sendMessage}
          closeChat={handleCloseChat}
          contactName={contactName}
        />
      ) : (
        <ContactsList 
          apiUrl={`/api/users/GetAllUsers?currentUserUsername=${currentUserUsername}`}
          heading="All Contacts"
          onJoinChat={handleJoinChat} 
        />
      )}
    </Flex>
  );
};

export default ContactsPage;
