import React from "react";
import { useSelector } from "react-redux";
import { Stack, Avatar, Heading, Text, Flex, Box } from "@chakra-ui/react";
import { RecentChatsMockData } from "../data/recentChatsMockData";
import { selectAccountUsername } from "../assets/AccountDataSlice";


export const Home = () => {
  const accountUsername = useSelector(selectAccountUsername); 
  const recentChats = RecentChatsMockData();

  return (
    <Flex direction="column" h="100vh">
      <Flex as="nav"  >
        <Box flex="1" align="center" justify="center" pt="10px">
    
          <Stack spacing={4}>
            {recentChats.map(chat => (
              <Box
                key={chat.id}
                p={4}
                borderWidth="1px"
                borderRadius="lg"
                w="full"
               
                cursor="pointer"
              >
                <Flex align="center">
                  <Avatar src={chat.avatarUrl} mr={4} />
                  <Box>
                    <Text fontWeight="bold">{chat.name}</Text>
                    <Text fontSize="sm" color="gray.500">{chat.latestMessage}</Text>
                  </Box>
                </Flex>
              </Box>
            ))}
          </Stack>
        </Box>
      </Flex>
    </Flex>
  );
};
