import React from 'react';
import { Box, Text, Button, Stack, Avatar, Heading, Divider, Flex } from '@chakra-ui/react';
import { useSelector } from 'react-redux';
import { selectAccountUsername } from '../assets/AccountDataSlice';

const Profile = () => {
  const currentUserUsername = useSelector(selectAccountUsername);

  return (
    <Flex align="center" justify="center" h="100vh" color="white">
      <Box
        p={8}
        shadow="lg"
        borderWidth="1px"
        borderRadius="lg"
        bg="#282E33"  
        maxW="400px"
        w="100%"
        textAlign="center"
        boxShadow="2xl"
      >
        <Avatar
          size="2xl"
          name={currentUserUsername}
          src="https://bit.ly/broken-link"  
          mb={4}
        />

        <Heading as="h2" size="lg" mb={2} color="teal.300">
          {currentUserUsername}
        </Heading>
        <Text fontSize="sm" color="gray.300" mb={4}>
          A passionate coder and tea lover ☕️
        </Text>

        <Divider mb={4} borderColor="gray.600" />
        <Stack spacing={3} textAlign="left">
          <Box>
            <Text fontWeight="bold" color="gray.400">
              Name:
            </Text>
            <Text fontSize="lg" color="white">
              John Doe 
            </Text>
          </Box>

          <Box>
            <Text fontWeight="bold" color="gray.400">
              Username:
            </Text>
            <Text fontSize="lg" color="white">
              {currentUserUsername}
            </Text>
          </Box>

          <Box>
            <Text fontWeight="bold" color="gray.400">
              Email:
            </Text>
            <Text fontSize="lg" color="white">
              johndoe@example.com 
            </Text>
          </Box>

          <Box>
            <Text fontWeight="bold" color="gray.400">
              Password:
            </Text>
            <Text fontSize="lg" color="white">
              ********
            </Text>
          </Box>
        </Stack>

        <Button mt={6} colorScheme="teal" variant="solid" size="lg" width="100%">
          Edit Profile
        </Button>
      </Box>
    </Flex>
  );
};

export default Profile;
