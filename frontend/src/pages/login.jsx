import {
  Flex,
  Heading,
  Input,
  Switch,
  useColorMode,
  useColorModeValue,
  FormControl,
  FormLabel,
  Button,
  Text,
  useToast,
  cookieStorageManager,
} from '@chakra-ui/react';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setAccountId, setAccountUsername } from '../assets/AccountDataSlice';
import api from '../api/api';


export const Login = () => {
  const { toggleColorMode } = useColorMode();
  const navigate = useNavigate();
  const formBackground = useColorModeValue('#EECAA6', 'gray.700');
  const toast = useToast();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();

  const redirectToRegistration = () => {
    navigate('/register');
  };

  const handleLogin = async () => {
    if (!email || !password) {
      toast({
        title: 'Error',
        description: 'Please fill in both fields.',
        status: 'error',
        duration: 3000,
        isClosable: true,
      });
      return;
    }

    try {
      const response = await api.post('/api/account/login', { email, password });

      if (response.data) {
        const userResponse = await api.get(`/api/users/GetByEmail/${response.data.email}`);
        console.log('User Response:', userResponse.data);
        console.log("AccountUsername: ",dispatch(setAccountUsername(userResponse.data.username)));
        console.log("accountId: ",dispatch(setAccountId(userResponse.data.userId)));
        navigate('/recentChats');
      }
    } catch (error) {
      toast({
        title: 'Login Error',
        description: error.response?.data?.message || 'An error occurred during login.',
        status: 'error',
        duration: 3000,
        isClosable: true,
      });
      console.error('Login error', error);
    }
  };

  return (
    <Flex h="100vh" alignItems="center" justifyContent="center">
      <Flex
        flexDirection="column"
        bg={formBackground}
        p={10}
        borderRadius={15}
        boxShadow="dark-lg"
        width="30%">
        <Heading mb={6}>Log In</Heading>
        <Input
          placeholder="email@gmail.com"
          type="email"
          variant="filled"
          onChange={(e) => setEmail(e.target.value)}
          mb={3}
          aria-label="Email"
          _focus={{
            bg: 'black',
            borderColor: '#2C7A7B'
          }}
        />
        <Input
          placeholder="Password"
          type="password"
          variant="filled"
          onChange={(e) => setPassword(e.target.value)}
          mb={6}
          _focus={{
            bg: 'black',
            borderColor: '#2C7A7B'
          }}
          aria-label="Password"
        />
        <Button textColor="black" colorScheme="teal" mb={10} onClick={handleLogin}>
          Login
        </Button>
        <Text textAlign="center">
          Not yet registered?
        </Text>
        <Button colorScheme="red" mt={4} onClick={redirectToRegistration}>
          Registration!
        </Button>
        <FormControl display="flex" alignItems="center" mt={4}>
          <FormLabel htmlFor="dark_mode" mb="0">
            Enable Dark Mode?
          </FormLabel>
          <Switch id="dark_mode" colorScheme="teal" size="lg" onChange={toggleColorMode} />
        </FormControl>
      </Flex>
    </Flex>
  );
};
