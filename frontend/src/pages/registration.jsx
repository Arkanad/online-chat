import React, { useState } from 'react';
import { Flex, Heading, Input, Switch, useColorMode, useToast, useColorModeValue, FormControl, FormLabel, Button, Show, Container } from '@chakra-ui/react';
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import api from '../api/api';


import { useDispatch } from 'react-redux';

import 'react-toastify/dist/ReactToastify.css';

export const Registration = () => {
  const navigate = useNavigate();
  const { toggleColorMode } = useColorMode();
  const formBackground = useColorModeValue('#EECAA6', 'gray.700');
  const toast = useToast();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const dispatch = useDispatch();

  const handleRegister = async () => {
    try {
      const response = await api.post("/api/account/register", {
        Username: username,
        Password: password,
        Email: email,
        Name: name
      });
      console.log(response.data);
      navigate("/home");
    } catch (error) {
      toast({
        title: 'Registration failed',
        description: error.response?.data?.message || 'An error occurred during registration.',
        status: 'error',
        duration: 3000,
        isClosable: true,
      });
      console.error('Registration error', error);
    }
  }

  return (
    <Flex h="100vh" alignItems="center" borderRadius={15} justifyContent="center">
      <Flex
        flexDirection="column"
        bg={formBackground}
        p={100}
        color="dark"
        borderRadius={15}
        boxShadow="dark-lg"
        width={'30%'}
      >
        <Heading colorScheme="red" mb={6}>Register</Heading>
        <Input
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          variant="filled"
          mb={3}
        />
        <Input
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          variant="filled"
          mb={3}
        />
        <Input
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          type="email"
          variant="filled"
          mb={3}
          Email
        />
        <Input
          placeholder="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          variant="filled"
          mb={6}
        />

        <Button
          textColor={'black'}
          type='button'
          colorScheme="teal"
          mb={8}
          onClick={handleRegister}
        >
          Register
        </Button>

        <Button
          variant="link"
          colorScheme="teal"
          onClick={handleRegister}
          mb={6}
        >
          Back to Login
        </Button>

        <FormControl alignContent={"center"} justifyContent={"center"} display="flex" mt={2}  >
          <FormLabel htmlFor="dark_mode">
            Enable Light Mode?
          </FormLabel>
          <Switch
            id="dark_mode"
            colorScheme="teal"
            size="lg"
            onChange={toggleColorMode}
          />
        </FormControl>
      </Flex>
    </Flex>
  );
};


export default Registration;
