
import { HubConnectionBuilder } from "@microsoft/signalr";
import { current } from "@reduxjs/toolkit";
import { useState } from "react";
import { useSelector } from "react-redux";
import { selectAccountUsername } from "../assets/AccountDataSlice";

let connection = null;


export const joinChat = async (contactId, currentUserUsername, chatRoom) => {
  
  // if (connection) {
  //   console.log("Already connected to a chat.");
  //   return;
  // }

  connection = new HubConnectionBuilder()
    .withUrl("http://localhost:5039/chat")
    .withAutomaticReconnect()
    .build();
    
  try {
    await connection.start();

    await connection.invoke("JoinChat", contactId, chatRoom, currentUserUsername);
    console.log(`${currentUserUsername} joined the chat: ${chatRoom}`);
  } catch (error) {
    console.error("Error joining chat:", error);
  }
};

export const closeChat = async () => {
  if (connection) {
    await connection.stop(); 
    connection = null;
  }
};

export const getConnection = () => connection;
