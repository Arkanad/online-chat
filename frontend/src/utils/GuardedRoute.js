import React from 'react';
import { Navigate, Outlet } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux'; // Import your action to reset auth statei
import Cookies from 'js-cookie';


const ProtectedRoutes = () => {
    const jwtToken = Cookies.get('.AspNetCore.Application.Id'); 

    if (jwtToken == null) {
        console.log("No JWT token found, resetting auth state");
    }

    return jwtToken ? <Outlet/> : <Navigate to="/login"/>
}

export default ProtectedRoutes;
